import java.io.IOException;
import java.util.Arrays;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SpiderWeb {
	
	private String[] results;
	private int i;
	private int n;
	
	public static void main(String[] args) throws IOException {
		SpiderWeb sw = new SpiderWeb( "http://www.google.com", 20, 1000);
		sw.sortResults();
	}
	
	public SpiderWeb(String root, int n, int t) throws IOException {
		results = new String[n];
		Document doc = Jsoup.connect( root ).timeout(t).get();
		Elements links = doc.select("a[href]");
		i = 0;
		for (Element link : links) {
			if (i >= n) {
				break;
			} else if (inResults(link.attr("href")) || link.attr("href").indexOf("http") == -1) { // does not include protocol
				continue;
			} else {
				results[i] = link.attr("href");	
				i++;
				crawl(link.attr("href"));
			}
		}
	}
	
	private void crawl(String page) throws IOException {
		Document doc_b = Jsoup.connect( page ).get();
		Elements links_b = doc_b.select("a[href]");
		for ( Element link_b: links_b) {
			if (i >= n) {
				return;
			} else {
				results[i] = link_b.attr("href");
				i++;
			}
		}
		return;
	}
	
	private void sortResults() {
		Arrays.sort(results);
        for(int i = 0; i < results.length; i++) {
        	int queryAt = results[i].indexOf('?');
        	if (queryAt != -1) {
        		// (queryAt + 1) omits the '?'
        		String fullQuery = results[i].substring(queryAt + 1);
        		results[i] = results[i].substring(0, queryAt);
            	System.out.println(results[i]);
        		String[] querySets = fullQuery.split("&");
        		for (int j = 0; j < querySets.length; j++) {
        			String[] quickSet = querySets[j].split("=");
            		System.out.println("	" + quickSet[0] + ": " + quickSet[1]);
        		}
        	}
        }
	}
	
	private boolean inResults( String test) {
		for (String result : results) {
			if (test.equals(result)) {
				return true;
			}
		}
		return false;
	}
	
}
