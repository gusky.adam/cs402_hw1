
var test = "GET HTTP:\/\/charity.cs.uwlax.edu\/a\/b?c=d&e=f#ghi HTTP\/1.1\nHost: charity.cs.uwlax.edu\nConnection: keep-alive\nPragma: no-cache\nCache-Control: no-cache\nUpgrade-Insecure-Requests: 1\nUser-Agent: Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/55.0.2883.95 Safari\/537.36\nAccept: text\/html,application\/xhtml+xml,application\/xml;q=0.9,image\/webp,*\/*;q=0.8\nAccept-Encoding: gzip, deflate, sdch\nAccept-Language: en-US,en;q=0.8,nb;q=0.6\n\nThis is the body";

var request = HttpRequest(test);

function HttpRequest( httpString ) {
  // create requestObject
  var req = new RequestObject(httpString);
  // console.log(JSON.stringify(req));
  console.log(req);

  return req;

  function RequestObject(oldStr) {
    this.arr = oldStr.split("\n");

    // Check for ERROR
    var regex = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/);
    if (!(this.arr[0].match(regex))) {
      console.log('Incorrect URL Format');
      return;
    }

    this.body = this.arr[this.arr.length - 1];
    var query = {};

    this.fragment = /\#\w+/.exec(this.arr[0])[0];
    this.arr[0] = this.arr[0].substring(0, /\#\w+/.exec(this.arr[0]).index);

    var queryString = /\?(\&?\w+\=\w+)+/.exec(this.arr[0])[0].substring(1).split("&");
    queryString.forEach(function(element) {
      var elementArr = element.split("=");
      query[elementArr[0]] = elementArr[1];
    });

    this.query = query;

    this.method = /[A-Z][A-Z]+\s/.exec(this.arr[0])[0];
    var protString = /\s\w+["S"]?/.exec(this.arr[0])[0];
    this.protocol = protString.substring(1);
    var hostString = /\:\/\/[\w+\.?]+\//.exec(this.arr[0])[0];
    this.host = hostString.substring(3, hostString.length - 1);

    if (this.protocol == "HTTP") {
      this.port = 80;
    } else {
      this.port = 443;
    }

    var pathString = /\/[\w\/?]+\?/.exec(this.arr[0])[0];
    this.path = pathString.substring(0, pathString.length - 1);

    this.url = this.arr[0].substring(this.arr[0].indexOf(this.path)) + this.fragment;

    // Removes mumbo jumbo from initial array --> left with only headers
    var onlyHeaders = this.arr;
    onlyHeaders.shift();
    onlyHeaders.pop();
    onlyHeaders.pop();
    var headers = {};
    onlyHeaders.forEach(function(element) {
      var elementArr = element.split(": ");
      headers[elementArr[0]] = elementArr[1];
    });
    this.headers = headers;
    delete this.arr;
  }
}
